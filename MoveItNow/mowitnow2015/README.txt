Le résultat sera généré dans le fichier "mower.log" via log4j2.
Cela est parametrable dans le fichier log4j2.xml se trouvant dans le répertoire "src/main/resources" du projet.

Les informations sont comme proposées par l'énoncé mises directement dans le test unitaire