package com.mowitnow.automaticmower.model;

public enum Direction {
	N, E, W, S
}
