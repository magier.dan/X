package com.mowitnow.automaticmower.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.awt.*;

public class AutomaticMower {

    public static final Logger logger = LogManager.getLogger(AutomaticMower.class);

    private Lawn lawn;
    private Point moverPosition;
    private Direction direction;
    private char[] instructions;

    public AutomaticMower(Point moverPosition, Direction direction, Lawn lawn) {
        setMoverPosition(moverPosition);
        setDirection(direction);
        setLawn(lawn);
    }

    public AutomaticMower(int x, int y, Direction direction, Lawn lawn) {
        this(new Point(x, y), direction, lawn);
    }

    public void processInstructions() {

        for (int i = 0; i < instructions.length; i++) {
            if (instructions[i] == 'A')
                move();
            if (instructions[i] == 'G')
                turnLeft();
            if (instructions[i] == 'D')
                turnRight();
        }

        showPosition();
    }

    public void move() {

        int xMowerActualPosition = moverPosition.x;
        int yMowerActualPosition = moverPosition.y;

        switch (direction) {
            case N:
                if (yMowerActualPosition < getLawn().getUpperRightCorner().getY()) {
                    moverPosition.move(xMowerActualPosition, yMowerActualPosition + 1);
                }
                break;
            case E:
                if (xMowerActualPosition < getLawn().getUpperRightCorner().getX()) {
                    moverPosition.move(xMowerActualPosition + 1, yMowerActualPosition);
                }
                break;
            case W:
                if (xMowerActualPosition > 0) {
                    moverPosition.move(xMowerActualPosition - 1, yMowerActualPosition);
                }
                break;
            case S:
                if (yMowerActualPosition > 0) {
                    moverPosition.move(xMowerActualPosition, yMowerActualPosition - 1);
                }
                break;
        }
    }

    private void turnLeft() {
        switch (direction) {
            case N:
                setDirection(Direction.W);
                logger.debug("Du Nord vars l'Ouest");
                break;
            case E:
                setDirection(Direction.N);
                logger.debug("De l'Est vers le Nord");
                break;
            case W:
                setDirection(Direction.S);
                logger.debug("De l'Ouest vers le Sud");
                break;
            case S:
                setDirection(Direction.E);
                logger.debug("Du Sud vers l'Est");
                break;
        }
    }

    private void turnRight() {
        switch (direction) {
            case N:
                setDirection(Direction.E);
                logger.debug("Du Nord vars l'Est");
                break;
            case E:
                setDirection(Direction.S);
                logger.debug("De l'Est vers le Sud");
                break;
            case W:
                setDirection(Direction.N);
                logger.debug("De l'Ouest vers le Nord");
                break;
            case S:
                setDirection(Direction.W);
                logger.debug("Du Sud vers l'Ouest");
                break;
        }
    }

    public String showPosition() {
        StringBuilder sb = new StringBuilder();

        sb.append(moverPosition.x);
        sb.append(" ");
        sb.append(moverPosition.y);
        sb.append(" ");
        sb.append(getDirection());

        return sb.toString();
    }

    /******************************************************************************************/

    public Lawn getLawn() { return lawn; }

    public void setLawn(Lawn lawn) { this.lawn = lawn; }

    public Point getMoverPosition() {
        return moverPosition;
    }

    public void setMoverPosition(Point moverPosition) {
        this.moverPosition = moverPosition;
    }

    public Direction getDirection() {
        return direction;
    }

    public void setDirection(Direction direction) {
        this.direction = direction;
    }

    public void setInstructions(char[] instructions) {
        this.instructions = instructions;
    }

    public char[] getInstructions() {
        return instructions;
    }
}
