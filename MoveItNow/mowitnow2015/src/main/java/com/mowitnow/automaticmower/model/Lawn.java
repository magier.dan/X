package com.mowitnow.automaticmower.model;

import java.awt.Point;

public class Lawn {

    private Point upperRightCorner;

    public Lawn(int x, int y) {
        this(new Point(x, y));
    }

    public Lawn(Point upperRightCorner) {
        this.upperRightCorner = upperRightCorner;
    }

    public Point getUpperRightCorner() {
        return upperRightCorner;
    }

    public void setUpperRightCorner(Point upperRightCorner) {
        this.upperRightCorner = upperRightCorner;
    }
}
