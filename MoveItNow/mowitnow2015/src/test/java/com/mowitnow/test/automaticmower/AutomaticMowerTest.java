package com.mowitnow.test.automaticmower;

import com.mowitnow.automaticmower.model.AutomaticMower;
import com.mowitnow.automaticmower.model.Direction;
import com.mowitnow.automaticmower.model.Lawn;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class AutomaticMowerTest {
    public static final Logger logger = LogManager.getLogger(AutomaticMowerTest.class);

    @Test
    public void testExecuteMowers() {

        List<AutomaticMower> automaticMowers = new ArrayList<>();

        Lawn lawn = new Lawn(5, 5);

        // 1 2 N
        // GAGAGAGAA
        // expected : 1 3 N
        AutomaticMower am1 = new AutomaticMower(1, 2, Direction.N, lawn);
        String instructions1 = "GAGAGAGAA";
        am1.setInstructions(instructions1.toCharArray());

        // 3 3 E
        // AADAADADDA
        // expected :  5 1 E
        AutomaticMower am2 = new AutomaticMower(3, 3, Direction.E, lawn);
        String instructions2 = "AADAADADDA";
        am2.setInstructions(instructions2.toCharArray());

        automaticMowers.add(am1);
        automaticMowers.add(am2);

        for (AutomaticMower mower : automaticMowers) {
            mower.processInstructions();
            logger.info(mower.showPosition());
        }


        Assert.assertEquals(new Point(1,3), am1.getMoverPosition());
        Assert.assertEquals(Direction.N, am1.getDirection());

        Assert.assertEquals(new Point(5,1), am2.getMoverPosition());
        Assert.assertEquals(Direction.E, am2.getDirection());
    }
}